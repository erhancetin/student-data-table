﻿

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
 <link rel="stylesheet" href="css/list/main.css">
     <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">


</head>

<style>
.form-control
{
  margin:0 auto;
}


body {
	font-family: 'Open Sans', sans-serif;
  background-color: #e7e9e8;
}
#container-item
{
  margin: 4rem auto;
}


</style>

<body>

<cfset asd='asd'>

<cfquery name="show_city" datasource="#asd#">
    SELECT * from City
    </cfquery>

    
<cfquery name="show_blood" datasource="#asd#">
    SELECT * from BloodGroup
    </cfquery>


<cfquery name="show_gsmcode" datasource="#asd#">
    SELECT * from GsmCode
    </cfquery>


<nav>
        <div id="logo-box">
            <a style="text-decoration:none;" href="http://127.0.0.1:8500/Datatable/index.cfm" class="logo ">
                    ÖĞRENCİ EKLE
             </a>
        </div>
    </nav>








  <div class="container-fluid ">
    
    <form id="container-item"action="query/add_student.cfm" method="post" enctype="multipart/form-data">

 <div id="form_div">
<div class="form-group">
				  <label for="fotodiv">Fotoğraf</label>
				  <div id="fotodiv">
					<input  type="file" class="form-control col-md-6" id="photopath"  name="photopath"  required data-error-msg="Foto Alanı Boş Geçilemez" >
				  </div>
				</div>



        <div class="form-group ">
          <label for="name">Adı</label>
          <input type="text" class="form-control col-md-6" name="s_name" id="name"  placeholder="Ad" required title="Adı Giriniz" data-error-msg="Adı Giriniz">
        
        </div>
        <div class="form-group">
          <label for="surname">Soyadı</label>
          <input type="text" class="form-control col-md-6" name="s_surname" id="surname"  placeholder="Soyad" required data-error-msg="Soyadı giriz">
         
        </div>

         

        
          <div class="form-group">
         
          <label class="d-block" for="phonecode">Phone Number:       </label>
          
          <select class="form-control col-md-1 d-inline-block f-2" id="phonecode" name="gsm_code" required data-error-msg="Bu alan boş geçilemez">
             <cfoutput query="show_gsmcode">
            <option value="#PhoneCodeId#">#PhoneCode#</option>
            </cfoutput>
            <option value=""Selected>Alan Kodu</option>
          </select>
            <input type="text" class="form-control col-md-4 d-inline-block f-l" id="phone"  placeholder="1234567"  name="phone" pattern="\d{7}" title="Telefon Numarası Hatalı" required data-error-msg="Bu alan boş geçilemez">
         
        </div>
        
        <div class="form-group"> <!-- Date input -->
          <label class="control-label" for="date">Date</label>
          <input class="form-control col-md-6" id="date" placeholder="MM/DD/YYY" type="text" name="birthdate" required data-error-msg="Bu alan boş geçilemez">
        </div>

         
   

   
        <div class="form-group">
          <label for="city">City</label>
          <select class="form-control col-md-6" id="city" name="city" required data-error-msg="Bu alan boş geçilemez" >
                <cfoutput query="show_city">
            <option value="#CityId#">#CityName#</option>
           </cfoutput>
            <option value=""Selected>Şehir Seçiniz</option>
          </select>
        </div>
        
        <div class="form-group">
          <label for="mail"> Mail</label>
          <input type="email" class="form-control col-md-6" id="mail" placeholder="Enter your Mail Address" name="email" required data-error-msg="Bu alan boş geçilemez">
        </div>

        <div class="form-group">
  <label for="homeadress">Ev Adresi</label>
  <textarea class="form-control col-md-6" rows="5" id="homeadress" name="adress" required data-error-msg="Bu alan boş geçilemez" autofocus></textarea>
</div>


       
        <div class="form-group">
          <label for="bloodtype">Blood Type</label>
          <select class="form-control col-md-6" id="bloodtype" name="blood" required data-error-msg="Bu alan boş geçilemez">
            <cfoutput query="show_blood">
            <option value="#BloodGroupID#">#BloodGroupName#</option>
           </cfoutput>
            <option value=""Selected>Kan Grubu</option>
          </select>
          
        </div>

      <div class="form-group">
          <label for="contactname">Emergency Name Surname</label>
          <input type="text" class="form-control col-md-6" id="contactname" placeholder="Emergency Name" name="contact_name_surname" required data-error-msg="Bu alan boş geçilemez" autofocus>
       
        </div>
   
        <div class="form-group">
         
          <label class="d-block" for="gsmcontactcode">Phone Number:       </label>
         
          <select class="form-control col-md-2 d-inline-block f-l" id="gsmcontactcode" name="contact_gsm_code" required data-error-msg="Bu alan boş geçilemez">
            <cfoutput query="show_gsmcode">
            <option value="#PhoneCodeId#">#PhoneCode#</option>
            </cfoutput>
            <option value=""Selected>Alan Kodu</option>
          </select>
            <input type="text" class="form-control col-md-4 d-inline-block f-l" id="gsmcontactphone"  placeholder="1234567" pattern="\d{7}" title="Telefon Numarası Hatalı"  name="contact_phone" required data-error-msg="Bu alan boş geçilemez">
         
        </div>


       
  <div class="form-group">
          <label for="contactmail">Emergency Mail</label>
          <input type="email" class="form-control col-md-6" id="contactmail" placeholder="Emergency Mail" name="contact_mail" required data-error-msg="Bu alan boş geçilemez">
        </div>
        
       
        <button type="submit" class="btn btn-primary btn-block">Ekle</button>
      </form>
      </div>
    </div>
  

<footer>
        <div id="after-Rooms">
        
     <div class="footer-item">
        <a href="http://127.0.0.1:8500/Datatable/student_list.cfm" class="f-btn primary-outline">
		   <i class="fas fa-home"></i> <p class="menutxt">Anasayfa</p>
	    </a>
    </div>

     <div  class="footer-item">
        <a href="http://127.0.0.1:8500/Datatable/index.cfm" class="f-btn primary-outline">
		   <i class="fas fa-user-plus"></i> <p class="menutxt">Ekle</p>
	    </a>
    </div>

     <div class="footer-item">
        <a href="http://127.0.0.1:8500/Datatable/student_list.cfm" class="f-btn primary-outline">
		   <i class="fas fa-user-times"></i> <p class="menutxt">Sil</p>
	    </a>
    </div>

     <div class="footer-item">
        <a href="http://127.0.0.1:8500/Datatable/student_list.cfm" class="f-btn primary-outline">
		   <i class="fas fa-user-edit"></i> <p class="menutxt">Güncelle</p>
	    </a>
    </div>
       
        </div>


    </footer>






  <script>
  $(document).ready(function(){
    var date_input=$('input[name="birthdate"]'); //our date input has the name "date"
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    var options={
      format:'yyyy-mm-dd',
      container: container,
      todayHighlight: true,
      autoclose: true,
    };
    date_input.datepicker(options);
  })
  </script>

</body>
</html>