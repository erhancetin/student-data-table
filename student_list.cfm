<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<style>

body {
	font-family: 'Open Sans', sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #333;
}

.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #4CAF50;
  color: white;
}

.topnav .icon {
  display: none;
}

@media screen and (max-width: 600px) {
  .topnav a:not(:first-child) {display: none;}
  .topnav a.icon {
    float: right;
    display: block;
  }
}

@media screen and (max-width: 600px) {
  .topnav.responsive {position: relative;}
  .topnav.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  .topnav.responsive a {
    float: none;
    display: block;
    text-align: left;
  }
}


#container
{
    width:95%;
    margin:5rem auto;
}

.menutxt {
    text-align: center;
}

footer {
    width: 100%;
    height: 4rem;
    margin: 0 auto;
    position: fixed;
    bottom: 0;
    z-index: 1;
}

.footer-item {
    width: 25%;
    height: 100%;
    float: left;
    height: 100%;
}

.primary-outline:hover {
    background-color: lightgray;
    color: #007bff;
    padding: .5rem .2rem;
    text-decoration: none;
}

.primary-outline {
    background-color: darkgray;
    color: #007bff;
    padding: .6rem .2rem;
}

#after-Rooms {
    width: 100%;
    height: 100%;
    margin-bottom: 0;
}

#after-Rooms i {
    float: left;
    width: 100%;
    font-size: 1.2rem;
    text-align: center;
}

.f-btn {
    border: none;
    text-decoration: none;
    font-size: 1.3rem;
    display: inline-block;
    width: 100%;
    height: 100%;
}

nav {
    width: 100%;
    position: fixed;
    top: 0;
    height: 4rem;
    background-color: gray;
    margin: 0 auto;
    z-index: 1;
}

#logo-box .logo {
    width: 100%;
    height: 100%;
    display: block;
    color: #fff;
    font-weight: 700;
    font-size: 2rem;
    text-align: center;
}

#logo-box {
    width: 80%;
    height: 100%;
    margin: auto;
    float:left;
}

#add_button
{
    width:20%;
    height:100%;
    float:left;
}

table tr {
  counter-increment: row-num;
}
table tr td:first-child::before {
    content: counter(row-num) ;
}



</style>



<body>

    <cfset asd='asd'>

    

        <cfquery name="show_student" datasource="#asd#">

            SELECT  s.StudentID,
                    s.StudentName,
                    s.StudentSurname, 
                    s.GSMCCode,
                    s.PhoneNumber,
                    s.Birthdate,
                    s.BirthCityId,
                    s.Email,
                    s.HomeAddress,
                    s.Photo,
                    s.BloodGroupID,
                    s.EmergencyContact_NameSurname,
                    s.EmergencyContact_GSMCCode,
                    s.EmergencyContact_PhoneNumber,
                    s.EmergencyContact_Email,
                    c.CityName,
                    b.BloodGroupName,
                    gsm.PhoneCode
                  
    from Students s 
    INNER JOIN City c on s.BirthCityId = c.CityId 
    INNER JOIN BloodGroup b on s.BloodGroupID=b.BloodGroupID 
    INNER JOIN GsmCode gsm on s.GSMCCode = gsm.PhoneCodeId 
            
        </cfquery>
        



        <nav>
        <div id="logo-box">
            <a style="text-decoration:none;" href="http://127.0.0.1:8500/Datatable/student_list.cfm" class="logo ">
                    ÖĞRENCİ LİSTESi
             </a>
        </div>
        <div id="add_button">
            <a class="f-r" href="http://127.0.0.1:8500/Datatable/index.cfm" target="_blank">
                                <button type="button" class="btn btn-primary btn-block btn-lg h-100">Ekle</button>
        </a>

        </div>

        
    </nav>


  <div id="container" >

        <table  id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Sıra</th>
                    <th>Fotoğraf</th>
                    <th>Adı</th>
                    <th>Soyadı</th>
                    <th>Telefon</th>
                    <th>E-Mail</th>
                    <th>   <a class="f-r" href="http://127.0.0.1:8500/Datatable/index.cfm" target="_blank">
                                <button type="button" class="btn btn-primary btn-block btn-lg h-100">Ekle</button>
        </a></th>
                    <th class="text-center"><i class="fas align-items-center fa-eye w-i fa-2x"></i></th>
                    <th class="text-center"><i class="fas fa-user-edit w-i fa-2x"></i></th>
                    <th class="text-center"><i class="fas fa-user-times w-i fa-2x"></i></th>
                </tr>
            </thead>
            <tbody>

                <cfoutput query="show_student">
               <cfset sira=0>
              
              
                    <tr>
                     
                        <td></td> 
                        <td><cfif len (Photo)><img src="#Photo#"></cfif></td>
                        <td>#StudentName#</td>
                        <td>#StudentSurname#</td>
                        <td>(#PhoneCode#) #PhoneNumber#</td>
                        <td>#Email#</td>
                         <td></td>

                        <td>
                            
                                <button type="button" class="btn btn-info btn-block"><i class="fas fa-eye w-i"></i></button>
                        
                        </td>

                        <td>
                            <a href="http://127.0.0.1:8500/Datatable/update_student.cfm?student_id=#StudentID#" target="_blank">
                                <button type="button" class="btn btn-secondary btn-block"><i class="fas fa-redo"></i></button>
                            </a>
                        </td>

                        <td>
                           
                        <button id="delete_button" value="#StudentID#" onclick="deleteFunction()" type="button" class="btn btn-danger btn-block"><i class="fas fa-trash-alt"></i></button>
                            
                        </td>

                    </tr>
                 </cfoutput>

                 

                 


                 
                

            </tbody>
            <tfoot>
                <tr>
                    <th>Sıra</th>
                     <th>Fotoğraf</th>
                    <th>Adı</th>
                    <th>Soyadı</th>
                    <th>Telefon</th>
                    <th>E-Mail</th>
                    <th>   <a class="f-r" href="http://127.0.0.1:8500/Datatable/index.cfm" target="_blank">
                                <button type="button" class="btn btn-primary btn-block btn-lg h-100">Ekle</button>
        </a></th>
                    <th class="text-center"><i class="fas fa-eye w-i fa-2x"></i></th>
                    <th class="text-center"><i class="fas fa-user-edit w-i fa-2x"></i></th>
                    <th class="text-center"><i class="fas fa-user-times w-i fa-2x"></i></th>
                </tr>
            </tfoot>
        </table>

        </div>

        <footer>
        <div id="after-Rooms">
        
     <div class="footer-item">
        <a href="http://127.0.0.1:8500/Datatable/student_list.cfm" class="f-btn primary-outline">
		   <i class="fas fa-home"></i> <p class="menutxt">Anasayfa</p>
	    </a>
    </div>

     <div  class="footer-item">
        <a href="http://127.0.0.1:8500/Datatable/index.cfm" class="f-btn primary-outline">
		   <i class="fas fa-user-plus"></i> <p class="menutxt">Ekle</p>
	    </a>
    </div>

     <div class="footer-item">
        <a href="http://127.0.0.1:8500/Datatable/student_list.cfm" class="f-btn primary-outline">
		   <i class="fas fa-user-times"></i> <p class="menutxt">Sil</p>
	    </a>
    </div>

     <div class="footer-item">
        <a href="http://127.0.0.1:8500/Datatable/student_list.cfm" class="f-btn primary-outline">
		   <i class="fas fa-user-edit"></i> <p class="menutxt">Güncelle</p>
	    </a>
    </div>
       
        </div>


    </footer>

        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>


        <script>
            $(document).ready(function() {
                $('#example').DataTable();
            });
        </script>

        <script>
            function deleteFunction() {
            var delete_value =document.getElementById('delete_button').value;
            if (confirm("Silmek istiyor musunuz ?")) {
               window.location.href='http://127.0.0.1:8500/Datatable/query/delete.cfm?student_id='+delete_value;
            } 
            }
           
            
        </script>
</body>

</html>