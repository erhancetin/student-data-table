﻿

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
     <link rel="stylesheet" href="css/list/main.css">
     <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    
    
</head>
<body>

<cfset asd='asd'>
<cfset asd='asd'>
   <cfquery name="get_update" datasource="#asd#">
    SELECT * from Students s 
    INNER JOIN City c on s.BirthCityId = c.CityId 
    INNER JOIN BloodGroup b on s.BloodGroupID=b.BloodGroupID 
    INNER JOIN GsmCode gsm on s.GSMCCode = gsm.PhoneCodeId 
    INNER JOIN GsmCode gsmm on s.GSMCCode = gsmm.PhoneCodeId 
   
    where s.StudentID=#URL.student_id# 
   
       </cfquery>

     

       <cfquery name="show_city" datasource="#asd#">
    SELECT * from City
    </cfquery>

    
<cfquery name="show_blood" datasource="#asd#">
    SELECT * from BloodGroup
    </cfquery>

<cfquery name="show_gsmcode" datasource="#asd#">
    SELECT * from GsmCode
    </cfquery>
      


       


<nav>
        <div id="logo-box">
            <a style="text-decoration:none;" href="http://127.0.0.1:8500/Datatable/student_list.cfm" class="logo ">
                    ÖĞRENCİ KAYIT
             </a>
        </div>
    </nav>



  <div class="container-fluid">
    
    <form action="query/update.cfm?student_id=<cfoutput>#get_update.StudentID#</cfoutput>" method="post">

    <cfoutput query="get_update">


    <div class="image">
										<cfif len(Photo)> <img src="#Photo#"></cfif>
									</div>


        <div class="form-group">
          <label for="name">Student Name</label>
          <input type="text" class="form-control col-md-4" name="s_name" id="name"  placeholder="Enter your name" value="#StudentName#" required data-error-msg="Bu alan boş geçilemez">
        
        </div>
        <div class="form-group">
          <label for="surname"></label>
          <input type="text" class="form-control col-md-4" name="s_surname" id="surname"  placeholder="Enter your surname" value="#StudentSurname#" required data-error-msg="Bu alan boş geçilemez">
         
        </div>

      
				  

        
          <div class="form-group">
         
          <label class="d-block" for="phonecode">Phone Number:       </label>
          <select class="form-control col-md-1 d-inline-block f-l" id="phonecode" name="gsm_code" required data-error-msg="Bu alan boş geçilemez">
            <option value="#GSMCCode#" selected>#PhoneCode#</option>
            </cfoutput>
               <cfoutput query="show_gsmcode">
            <option value="#PhoneCodeId#">#PhoneCode#</option>
            </cfoutput>
          </select>
          <cfoutput query="get_update">
            <input type="text" class="form-control col-md-3 d-inline-block f-l" id="phone"  placeholder="Enter your phone"  name="phone" value="#PhoneNumber#" required data-error-msg="Bu alan boş geçilemez">
         
        </div>
        
        <div class="form-group"> <!-- Date input -->
          <label class="control-label" for="date">Date</label>
          <input class="form-control col-md-4" id="date" placeholder="MM/DD/YYY" type="text" name="birthdate" value="#Birthdate#" required data-error-msg="Bu alan boş geçilemez">
        </div>
   
        <div class="form-group">
          <label for="city">City</label>
          <select class="form-control col-md-4" id="city" name="city" value="#BirthCityId#" required data-error-msg="Bu alan boş geçilemez">
            <option value="#BirthCityId#" selected>#CityName#</option>
        </cfoutput>
          
           <cfoutput query="show_city">
             <option value="#CityId#">#CityName#</option>
          </cfoutput>
           
          </select>
        </div>
            <cfoutput query="get_update">


        <div class="form-group">
          <label for="mail"> Mail</label>
          <input type="email" class="form-control col-md-4" id="mail" placeholder="Enter your Mail Address" name="email" value="#Email#" required data-error-msg="Bu alan boş geçilemez">
        </div>

        <div class="form-group">
  <label for="homeadress">Ev Adresi</label>
  <textarea class="form-control col-md-4" rows="5" id="homeadress" name="adress" required data-error-msg="Bu alan boş geçilemez">#HomeAddress#</textarea>
</div>






<div class="form-group">
				  <label for="fotodiv">Fotoğraf</label>
				  <div id="fotodiv">
					<input type="file" class="form-control col-md-4" id="foto" placeholder="Photo" name="photo" value="" required data-error-msg="Bu alan boş geçilemez">
				  </div>
				</div>
       
        <div class="form-group">
          <label for="bloodtype">Blood Type</label>
          <select class="form-control col-md-4" id="bloodtype" name="blood" value="#BloodGroupID#" required data-error-msg="Bu alan boş geçilemez">
            <option value="#BloodGroupID#" selected>#BloodGroupName#</option>
         </cfoutput>

            <cfoutput query="show_blood">
            <option value="#BloodGroupID#">#BloodGroupName#</option>
         </cfoutput>
            
            <cfoutput query="get_update">
          </select>
        </div>
        <div class="form-group">
          <label for="contactname">Emergency Name Surname</label>
          <input type="text" class="form-control col-md-4" id="contactname" placeholder="Emergency Name" name="contact_name_surname" value="#EmergencyContact_NameSurname#">
       
        </div>
        
   
        <div class="form-group">
         
          <label class="d-block"for="gsmcontactcode">Phone Number:       </label>
          
          <select class="form-control col-md-1 d-inline-block f-l" id="gsmcontactcode" name="contact_gsm_code" required data-error-msg="Bu alan boş geçilemez">
             <option value="#EmergencyContact_GSMCCodeId#" selected>#PhoneCode#</option> 
             </cfoutput>
            <cfoutput query="show_gsmcode">
            <option value="#PhoneCodeId#">#PhoneCode#</option>
            </cfoutput>
          </select>
          <cfoutput query="get_update">
					
            <input type="text" class="form-control col-md-3 d-inline-block f-l" id="gsmcontactphone"  placeholder="Enter your phone"  name="contact_phone" value="#EmergencyContact_PhoneNumber#" required data-error-msg="Bu alan boş geçilemez" >
         
        </div>

        <div class="form-group">
          <label for="contactmail">Emergency Mail</label>
          <input type="email" class="form-control col-md-4" id="contactmail" placeholder="Emergency Mail" name="contact_mail" value="#EmergencyContact_Email#" required data-error-msg="Bu alan boş geçilemez">
        </div>
        </cfoutput>
       
        <button type="submit" class="btn btn-primary btn-block">Güncelle</button>
      </form>
    </div>
  </div>    
 </div>


 <footer>
        <div id="after-Rooms">
        
     <div class="footer-item">
        <a href="http://127.0.0.1:8500/Datatable/student_list.cfm" class="f-btn primary-outline">
		   <i class="fas fa-home"></i> <p class="menutxt">Anasayfa</p>
	    </a>
    </div>

     <div  class="footer-item">
        <a href="http://127.0.0.1:8500/Datatable/index.cfm" class="f-btn primary-outline">
		   <i class="fas fa-user-plus"></i> <p class="menutxt">Ekle</p>
	    </a>
    </div>

     <div class="footer-item">
        <a href="http://127.0.0.1:8500/Datatable/student_list.cfm" class="f-btn primary-outline">
		   <i class="fas fa-user-times"></i> <p class="menutxt">Sil</p>
	    </a>
    </div>

     <div class="footer-item">
        <a href="http://127.0.0.1:8500/Datatable/student_list.cfm" class="f-btn primary-outline">
		   <i class="fas fa-user-edit"></i> <p class="menutxt">Güncelle</p>
	    </a>
    </div>
       
        </div>


    </footer>





<script>
      $(document).ready(function(){
        var date_input=$('input[name="birthdate"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        var options={
          format: 'yyyy-mm-dd',
          container: container,
          todayHighlight: true,
          autoclose: true,
        };
        date_input.datepicker(options);
      })
  </script>


</body>
</html>